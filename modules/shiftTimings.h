//
// Created by Aaron Russo on 18/07/16.
//

#ifndef FIRMWARE_SHIFTTIMINGS_H
#define FIRMWARE_SHIFTTIMINGS_H

#include "d_can.h"
#include "../libs/eeprom.h"

//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////
//Following timings are in ms
//Default Timings
#define TIME_UP_PUSH_DELAY      20   //Ritardo prima del push (cut)
#define TIME_UP_PUSH_1TO2       115
#define TIME_UP_PUSH_2TO3       100
#define TIME_UP_PUSH_3TO4       100
#define TIME_UP_PUSH_4TO5       100
#define TIME_UP_REBOUND         15
#define TIME_UP_BRAKE           20

#define TIME_DOWN_CLUTCH        70     //Tempo attesa in cui tira soltanto frizione
#define TIME_DOWN_PUSH          100
#define TIME_DOWN_REBOUND       25
#define TIME_DOWN_BRAKE         15

//Neutral Timings
#define TIME_NEUTRAL_UP_CLUTCH          300
#define TIME_NEUTRAL_UP_PUSH            22
#define TIME_NEUTRAL_UP_REBOUND         15
#define TIME_NEUTRAL_UP_BRAKE           35

#define TIME_NEUTRAL_DOWN_CLUTCH        300
#define TIME_NEUTRAL_DOWN_PUSH          25
#define TIME_NEUTRAL_DOWN_REBOUND       15
#define TIME_NEUTRAL_DOWN_BRAKE         35
//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////

#define ID_TIME_UP_PUSH_DELAY   0
#define ID_TIME_UP_PUSH 1
#define ID_TIME_UP_REBOUND  2
#define ID_TIME_UP_BRAKE    3
#define ID_TIME_DOWN_CLUTCH 4
#define ID_TIME_DOWN_PUSH   5
#define ID_TIME_DOWN_REBOUND    6
#define ID_TIME_DOWN_BRAKE  7
#define ID_TIME_NEUTRAL_UP_CLUTCH   8
#define ID_TIME_NEUTRAL_UP_PUSH 9
#define ID_TIME_NEUTRAL_UP_REBOUND  10
#define ID_TIME_NEUTRAL_UP_BRAKE    11
#define ID_TIME_NEUTRAL_DOWN_CLUTCH 12
#define ID_TIME_NEUTRAL_DOWN_PUSH   13
#define ID_TIME_NEUTRAL_DOWN_REBOUND    14
#define ID_TIME_NEUTRAL_DOWN_BRAKE  15

#define SHIFTING_TIMINGS_COUNT  16
#define EEPROM_STARTING_ADDRESS 20

void ShiftTimings_injectCommand(unsigned int command, unsigned int timingID, unsigned int value);

void ShiftTimings_change(unsigned int timingID, unsigned int value);

void ShiftTimings_save(void);

void ShiftTimings_load(void);

void ShiftTimings_loadDefaults(void);

void _ShiftTimings_confirmChange(unsigned int timingID, unsigned int value);

void _ShiftTimings_confirmSave(void);

char _ShiftingTimings_areTimingsSaved(void);


#endif //FIRMWARE_SHIFTTIMINGS_H