//
// Created by Aaron Russo on 18/07/16.
//

#include "shiftTimings.h"

unsigned int shiftingTimings_values[SHIFTING_TIMINGS_COUNT];

void ShiftTimings_injectCommand(unsigned int command, unsigned int timingID, unsigned int value) {
    if (command == CAN_COMMAND_CHANGE_SHIFT_TIMING) {
        ShiftTimings_change(timingID, value);
    } else if (command == CAN_COMMAND_SAVE_SHIFT_TIMINGS) {
        ShiftTimings_save();
    }
}

void ShiftTimings_change(unsigned int timingID, unsigned int value) {
    shiftingTimings_values[timingID] = value;
    _ShiftTimings_confirmChange(timingID, value);
}

void ShiftTimings_save(void) {
    EEPROM_writeArray(EEPROM_STARTING_ADDRESS, shiftingTimings_values);
    _ShiftTimings_confirmSave();
}

void ShiftTimings_load(void) {
    unsigned int i;
    EEPROM_readArray(EEPROM_STARTING_ADDRESS, shiftingTimings_values);
    if (!_ShiftingTimings_areTimingsSaved()) {
        ShiftTimings_loadDefaults();
    }
    for (i = 0; i < SHIFTING_TIMINGS_COUNT; i += 1) {
        _ShiftTimings_confirmChange(i, shiftingTimings_values[i]);
        Delay_ms(10);
    }
}

void ShiftTimings_loadDefaults(void) {
    shiftingTimings_values[ID_TIME_UP_PUSH_DELAY] = TIME_UP_PUSH_DELAY;
    shiftingTimings_values[ID_TIME_UP_PUSH] = TIME_UP_PUSH;
    shiftingTimings_values[ID_TIME_UP_REBOUND] = TIME_UP_REBOUND;
    shiftingTimings_values[ID_TIME_UP_BRAKE] = TIME_UP_BRAKE;
    shiftingTimings_values[ID_TIME_DOWN_CLUTCH] = TIME_DOWN_CLUTCH;
    shiftingTimings_values[ID_TIME_DOWN_PUSH] = TIME_DOWN_PUSH;
    shiftingTimings_values[ID_TIME_DOWN_REBOUND] = TIME_DOWN_REBOUND;
    shiftingTimings_values[ID_TIME_DOWN_BRAKE] = TIME_DOWN_BRAKE;
    shiftingTimings_values[ID_TIME_NEUTRAL_UP_CLUTCH] = TIME_NEUTRAL_UP_CLUTCH;
    shiftingTimings_values[ID_TIME_NEUTRAL_UP_PUSH] = TIME_NEUTRAL_UP_PUSH;
    shiftingTimings_values[ID_TIME_NEUTRAL_UP_REBOUND] = TIME_NEUTRAL_UP_REBOUND;
    shiftingTimings_values[ID_TIME_NEUTRAL_UP_BRAKE] = TIME_NEUTRAL_UP_BRAKE;
    shiftingTimings_values[ID_TIME_NEUTRAL_DOWN_CLUTCH] = TIME_NEUTRAL_DOWN_CLUTCH;
    shiftingTimings_values[ID_TIME_NEUTRAL_DOWN_PUSH] = TIME_NEUTRAL_DOWN_PUSH;
    shiftingTimings_values[ID_TIME_NEUTRAL_DOWN_REBOUND] = TIME_NEUTRAL_DOWN_REBOUND;
    shiftingTimings_values[ID_TIME_NEUTRAL_DOWN_BRAKE] = TIME_NEUTRAL_DOWN_BRAKE;
}

void _ShiftTimings_confirmSave(void) {
    Can_writeInt(GCU_CLUTCH_ID, CAN_COMMAND_CONFIRM_SHIFT_TIMING_SAVE);
}

void _ShiftTimings_confirmChange(unsigned int timingID, unsigned int value) {
    Can_resetWritePacket();
    Can_addIntToWritePacket(CAN_COMMAND_CONFIRM_SHIFT_TIMING_CHANGE);
    Can_addIntToWritePacket(timingID);
    Can_addIntToWritePacket(value);
    Can_write(GCU_CLUTCH_ID);
}

char _ShiftingTimings_areTimingsSaved(void) {
    unsigned int i, defaultChecker;
    char areSaved;
    areSaved = FALSE;
    defaultChecker = shiftingTimings_values[0];
    for (i = 1; i < SHIFTING_TIMINGS_COUNT; i += 1) {
        if (defaultChecker != shiftingTimings_values[i]) {
            areSaved = TRUE;
            break;
        }
    }
    return areSaved;
}