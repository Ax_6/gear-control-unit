/*
*/

#define LIMIT_SEND            50   //tentativi di trasmettere

//variabili scritte su CAN, possono essere globali o locali nella funzione che chiama la write
unsigned char fanSense=0;
unsigned char fuelPumpSense=0;
unsigned char H2OpumpSense=0;
unsigned char fanDutyCycle=0;
unsigned char H2OpumpDutyCycle=0;
unsigned int  clutchFeedback=0;
unsigned char gearMotorState=0;
unsigned char KILLstate=0;

//variabili lette dal CAN, devono rimanere globali
unsigned int  brakePfront=0;
unsigned int  brakePrear=0;
unsigned int  steerAngle=0;
unsigned int  clutchPosSensor=0;
unsigned char gearshift=0;
unsigned int  clutchTarget=0;
unsigned char fire=0;
unsigned char launch=0;
//anche alcune variabili della EFI, definire meglio quali

//variabili funzionamento CAN
char CAN_datain[8]={0,0,0,0,0,0,0,0};
unsigned int CAN_ricevuto=0;
int CAN_flags=0;
unsigned long int CAN_id=0;
unsigned int CAN_dataLen=0;


void CAN_DP8_GCU_Init(){
     unsigned int Can_Init_flags = 0;
     Can_Init_flags = _CAN_CONFIG_STD_MSG &             // standard identifier 11 bit
                      _CAN_CONFIG_DBL_BUFFER_ON &       // double buffer mode
                      _CAN_CONFIG_MATCH_MSG_TYPE &
                      _CAN_CONFIG_LINE_FILTER_ON &      // wake up by line
                      _CAN_CONFIG_SAMPLE_THRICE &       // for robustness
                      _CAN_CONFIG_PHSEG2_PRG_ON;        // these last two are linked to sync
     CAN1Initialize(2,4,3,4,2,Can_Init_flags);          // SJW,BRP,PHSEG1,PHSEG2,PROPSEG
     CAN1SetOperationMode(_CAN_MODE_CONFIG,0xFF);
     
     CAN1SetMask(_CAN_MASK_B1, 0b00000000000, _CAN_CONFIG_MATCH_MSG_TYPE & _CAN_CONFIG_STD_MSG);        // DA CONFIGURARE
     CAN1SetFilter(_CAN_FILTER_B1_F1, 0, _CAN_CONFIG_STD_MSG);                                          // DA CONFIGURARE
     CAN1SetFilter(_CAN_FILTER_B1_F2, 0, _CAN_CONFIG_STD_MSG);                                          // DA CONFIGURARE
     
     CAN1SetMask(_CAN_MASK_B2, 0b00000000000, _CAN_CONFIG_MATCH_MSG_TYPE & _CAN_CONFIG_STD_MSG);        // DA CONFIGURARE
     CAN1SetFilter( _CAN_FILTER_B2_F1, 0, _CAN_CONFIG_STD_MSG);                                         // DA CONFIGURARE
     
     CAN1SetOperationMode(_CAN_MODE_NORMAL,0xFF);
     
     //Interrupt for CAN1, catch with   void functionname iv ADDR_C1INTERRUPT
     //clear flag IFS1BITS.C1IF=0;
     INTERRUPT_PROTECT(IEC1BITS.C1IE=1);
     INTERRUPT_PROTECT(C1INTEBITS.RXB0IE=1); // DA RIVEDERE an interrupt is generated everytime that a message pass through the mask in buffer 0
}

void CAN_GCU_WRITE_SENSE(unsigned char fanSense, unsigned char fuelPumpSense, unsigned char H2OpumpSense, unsigned char fanDutyCycle, unsigned char H2OpumpDutyCycle){
     short int sent;
     unsigned char message[5];
     unsigned int tx_flags;
     int i=0;

     message[0] = fanSense;
     message[1] = fuelPumpSense;
     message[2] = H2OpumpSense;
     message[3] = fanDutyCycle;
     message[4] = H2OpumpDutyCycle;

     tx_flags=_CAN_TX_PRIORITY_2 &       //RIVEDERE LE PRIORITA'
              _CAN_TX_STD_FRAME  &
              _CAN_TX_NO_RTR_FRAME;

     do{
        sent=CAN1Write(GCU_SENSE_ID, message, 5, tx_flags);
        i++;
        }
     while((sent==0)&&(i<LIMIT_SEND));
     if (i==LIMIT_SEND) can_err++;
}

void dCan_GcuWriteClutch(unsigned int valore){
     short int sent;
     unsigned int tx_flags;
     int i=0;

     tx_flags=_CAN_TX_PRIORITY_1 &       //RIVEDERE LE PRIORITA'
              _CAN_TX_STD_FRAME  &
              _CAN_TX_NO_RTR_FRAME;

     do{
        sent=CAN1Write(GCU_CLUTCH_ID, valore, sizeof(valore), tx_flags);
        i++;
        }
     while((sent==0)&&(i<LIMIT_SEND));
     if (i==LIMIT_SEND) can_err++;
}

void CAN_GCU_WRITE_MOTOR_STATE(unsigned char valore){
     short int sent;
     unsigned int tx_flags;
     int i=0;

     tx_flags=_CAN_TX_PRIORITY_1 &       //RIVEDERE LE PRIORITA'
              _CAN_TX_STD_FRAME  &
              _CAN_TX_NO_RTR_FRAME;

     do{
        sent=CAN1Write(GCU_MOTOR_ID, valore, sizeof(valore), tx_flags);
        i++;
        }
     while((sent==0)&&(i<LIMIT_SEND));
     if (i==LIMIT_SEND) can_err++;
}

void CAN_GCU_WRITE_KILL(unsigned char valore){
     short int sent;
     unsigned int tx_flags;
     int i=0;

     tx_flags=_CAN_TX_PRIORITY_1 &       //RIVEDERE LE PRIORITA'
              _CAN_TX_STD_FRAME  &
              _CAN_TX_NO_RTR_FRAME;

     do{
        sent=CAN1Write(GCU_KILL_ID, valore, sizeof(valore), tx_flags);
        i++;
        }
     while((sent==0)&&(i<LIMIT_SEND));
     if (i==LIMIT_SEND) can_err++;
}

void CAN_GCU_WRITE_AUX(int valore){                  //funzione ausiliaria, da usare in caso di necessit�
     short int sent;
     unsigned char message[2];                       //aggiornare il numero di byte
     unsigned int tx_flags;
     int i=0;

     tx_flags=_CAN_TX_PRIORITY_2 &       //RIVEDERE LE PRIORITA'
              _CAN_TX_STD_FRAME  &
              _CAN_TX_NO_RTR_FRAME;

     do{
        sent=CAN1Write(CAN_AUX_ID, valore, sizeof(valore), tx_flags);  //aggiornare il numero di byte
        i++;
        }
     while((sent==0)&&(i<LIMIT_SEND));
     if (i==LIMIT_SEND) can_err++;
}





//CAN_DP8_GCU_READ

void CAN_Interrupt() iv IVT_ADDR_C1INTERRUPT{

}