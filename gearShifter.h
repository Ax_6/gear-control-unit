#define IN1 LATC1_bit
#define IN2 LATB0_bit

#define periodo_in_tick 39063 // PERIODO TIMER2 UGUALE A 500ms (FATTO VERIFICATO AL 2-7-2016)
#define periodo_in_ms 50 //

//SERVO KST ULTRAGIANT
#define MIN_KST_DUTY_CYCLE 1500 // DUTY CYCLE MINIMO KST ULTRAGIANT MISURATO IN TICK DEL TIMER3
#define MAX_KST_DUTY_CYCLE 6000 // DUTY CYCLE MINIMO KST ULTRAGIANT MISURATO IN TICK DEL TIMER3

//DC MOTOR
#define TEMPO_RIMBALZO 20 //[ms]
#define TEMPO_SPINTA 50 //[ms]
#define TEMPO_TIRO 50 //[ms]

/*LEGENDA gearMotorState
          0 - MOTORE FRENATO, PRONTO
          1 - MOTORE IN SPINTA, OCCUPATO
          2 - MOTORE IN TIRO, OCCUPATO
          3 - MOTORE IN RIMBALZO, OCCUPATO
*/

/*LEGENDA gearshift
          0 - MOTORE FRENATO, PRONTO
          1 - ROUTINE DI UP
          2 - ROUTINE DI DOWN
*/


//variabili per conversione da millisecondi a tick del timer
float ms_to_tick = 0;


void gearshifter_init() {
    ms_to_tick = periodo_in_tick / periodo_in_ms;
}

int spingi(int tempo_spinta) {
    if (tempo_spinta >= 500) {
        return 4;
    }
    PR2 = (int) tempo_spinta * ms_to_tick;
    INHIBIT = 1;
    IN1 = 1;
    IN2 = 0;
    T2CONbits.TON = 1;
    return 1;
}

int tira(int tempo_tiro) {
    if (tempo_tiro >= 500) {
        return 4;
    }
    PR2 = (int) tempo_tiro * ms_to_tick;
    INHIBIT = 1;
    IN1 = 0;
    IN2 = 1;
    T2CONbits.TON = 1;
    return 2;
}

int rimbalza(int tempo_rimbalzo) {
    if (tempo_rimbalzo >= 500) {
        return 4;
    }
    PR2 = (int) tempo_rimbalzo * ms_to_tick;
    INHIBIT = 0;
    IN1 = 0;
    IN2 = 0;
    T2CONbits.TON = 1;
    return 3;
}

int libero() {
    INHIBIT = 0;
    IN1 = 0;
    IN2 = 0;
    T2CONbits.TON = 0;
    return 4;
}

int frena_sempre() {
    //LED1_DEBUG=1;
    INHIBIT = 1;
    IN1 = 0;
    IN2 = 0;
    T2CONbits.TON = 0;
    return 0;
}

int tira_frizione(float frazione_tiro, int tempo_tiro) {
    int duty_in_tick = (int) (frazione_tiro * (MAX_KST_DUTY_CYCLE - MIN_KST_DUTY_CYCLE - 1) + MIN_KST_DUTY_CYCLE);
    if (duty_in_tick > MAX_KST_DUTY_CYCLE || duty_in_tick < MIN_KST_DUTY_CYCLE) {
        return 1;
    }
    OC8RS = duty_in_tick;
    PR5 = (int) (tempo_tiro * ms_to_tick);
    return 0;
}

void Timer2Interrupt()iv IVT_ADDR_T2INTERRUPT{
    T2IF_bit = 0;
    switch (gearMotorState) {
        case 1 : { //IL MOTORE HA APPENA SMESSO DI SPINGERE
            gearMotorState = rimbalza(TEMPO_RIMBALZO);
        }
        case 2 : { //IL MOTORE HA APPENA SMESSO DI TIRARE
            gearMotorState = rimbalza(TEMPO_RIMBALZO);
        }
        case 3 : { //IL MOTORE HA APPENA SMESSO DI RIMBALZARE
            gearMotorState = frena_sempre();
        }
        case 4 : { //IL MOTORE E� IN ERRORE
            gearMotorState = libero();
        }
        default : { //METTO IL MOTORE IN ERRORE
            gearMotorState = libero();
        }
    }
}